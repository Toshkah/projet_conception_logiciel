import unittest
from unittest.mock import MagicMock, patch
from app.model.mail import Mail


class TestMail(unittest.TestCase):
    
    def setUp(self):
        self.mail = Mail("from@example.com", "to@example.com", "Test Subject", "Test message", "2022-06-30", "queued", 1)
    
    @patch('smtplib.SMTP_SSL')
    def test_send_success(self, mock_smtp):
        cursor_mock = MagicMock()
        connection_mock = MagicMock()
        connection_mock.commit = MagicMock()
        cursor_mock.connection = connection_mock
        self.mail.send(cursor_mock)
        self.assertEqual(self.mail.state, "sent")
        cursor_mock.execute.assert_called_with('UPDATE mail SET statut = ? WHERE id = ?', ("sent", self.mail.id))
        mock_smtp.assert_called_with('smtp.gmail.com', 465)
    
    @patch('smtplib.SMTP_SSL')
    def test_send_failure(self, mock_smtp):
        mock_smtp.side_effect = Exception("SMTP error")
        cursor_mock = MagicMock()
        connection_mock = MagicMock()
        connection_mock.commit = MagicMock()
        cursor_mock.connection = connection_mock
        self.mail.send(cursor_mock)
        self.assertEqual(self.mail.state, "error")
        cursor_mock.execute.assert_called_with('UPDATE mail SET statut = ? WHERE id = ?', ("error", self.mail.id))
    
if __name__ == '__main__':
    unittest.main()
