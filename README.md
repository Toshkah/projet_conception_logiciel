## Presentation 

This package allows allows to plan the sending of mails while storing the requests of mails in order to keep in memory the state of the mails (sent, waiting, error).

__Note that it requires to have a gmail address__.


## Usage


__Installing required packages :__
Start by installing required Python packages using the following command :
```bash
pip install -r requirements
```

__Starting the app :__
To launch the app, use the following command inside of the repository folder :
```bash
python main.py
```

 __Using the API :__
The API provides 3 requests :

 - ```GET``` on ```/mail/{id}```, which displays information about mail n°```id```.
 - ```POST``` on ```/post_mail/```, which allows to add a mail to the database.
  - ```GET``` on ```/mails/```, which displays information about all mails.

  
To write a email, make sure it is of this format :
```json
{   
    "send_from": "sorry.forlate@gmail.com",
    "send_to": "no.idea@somemail.com",
    "subject": "An interesting subject",
    "msg": "Some words which can have no sense \nGreeting",
    "date": "Fri Mar 24 22:00:00 2023"
}
```

Note that ```"date"``` like __"%a %b %d %H:%M:%S %Y"___ 


## Configuration

To configure the package, you can edit ```.env```. Here, you can set the time period of the mail sending process and your mail password. For example, if you choose ```wait_time = 10```, the API will check every 10 seconds if a mail send date has been reached, and if so, it will send it.
The two other parameters are the host and port on which the API will run. 


## Architecture diagramm

![alt text](https://gitlab.com/Toshkah/projet_conception_logiciel/-/raw/main/readmeUtils/329903663_185484074238628_7242738257432512157_n.png)
