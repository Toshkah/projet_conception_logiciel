from app.dao.db_init import DBInit
from api import app
import requests
import json
from fastapi.testclient import TestClient
import uvicorn
from datetime import datetime
from app.model.mail import Mail

if __name__ == '__main__':
    uvicorn.run(app)