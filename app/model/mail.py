import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from app.dao.db_connection import DBConnection
from datetime import datetime
from dotenv import load_dotenv

load_dotenv() 
PASSWORD = os.environ.get('PASSWORD')

class Mail:
    """Cette classe est utilisée pour envoyer des e-mails à l'utilisateur.
    Attributs :
        send_to : l'adresse e-mail du destinataire.
        send_from : l'adresse e-mail de l'expéditeur.
        subject : le subject de l'e-mail.
        message : le contenu de l'e-mail.
        date : la date d'envoi de l'e-mail.
        state : l'état de l'e-mail.
        
    L'e-mail est envoyé lorsque la date est atteinte.
    """
    def __init__(self, send_from, send_to, subject, message, date, state='waiting', id=None):
        self.send_from = send_from
        self.send_to = send_to
        self.subject = subject
        self.message = message
        self.date = date
        self.state = state
        self.id = id
        
        
    def toString(self):
        return f"From: {self.send_from}\nTo: {self.send_to}\nDate:{self.date}\nSubject: {self.subject}\n\n{self.message}"
        

         
    def send(self, cursor):
        """
        It sends an email and triggers a notification if the email fails to send.
        """
        if self.message is None:
            self.message = ""
        mail_to_send = MIMEMultipart()
        mail_to_send['From'] = self.send_from
        mail_to_send['Subject'] = self.subject
        mail_to_send['To'] = self.send_to
        text_part = MIMEText(self.message, 'plain')        

        mail_to_send.attach(text_part)

        
        try:
            smtp_server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
            smtp_server.login(self.send_from, PASSWORD)
        

            smtp_server.sendmail(self.send_from, [self.send_to], mail_to_send.as_string()) # pass mail_to_send content
            smtp_server.quit()
            self.state = "sent"
            cursor.execute('UPDATE mail SET statut = ? WHERE id = ?', (self.state, self.id))
        except Exception as e:
            error_message = f"Error sending email: {str(e)}"
            print(error_message)
            self.state = "error"
            cursor.execute('UPDATE mail SET statut = ? WHERE id = ?', (self.state, self.id))
        finally:
            cursor.connection.commit()
