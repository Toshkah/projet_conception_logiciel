import sqlite3 as sl
from app.utils.singleton import Singleton


class DBConnection:
    """
    Technical class to open only one connection to the DB.
    """

    def __init__(self):
        self.__connection = sl.connect('app/dao/database.db')

    @property
    def connection(self):
        '''This function returns the connection object.

        Returns
        -------
            The connection object.
        '''
        return self.__connection
