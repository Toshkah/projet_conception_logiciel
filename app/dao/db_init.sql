DROP TABLE IF EXISTS mail;
CREATE TABLE mail (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    send_from TEXT NOT NULL,
    send_to TEXT NOT NULL,
    subject TEXT NOT NULL,
    msg TEXT NOT NULL,
    post_date DATETIME NOT NULL,
    statut TEXT NOT NULL
);

