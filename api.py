from fastapi import FastAPI
from dotenv import load_dotenv
from pydantic import BaseModel
from app.dao.db_connection import DBConnection
from app.model.mail import Mail
from datetime import datetime
from fastapi_utils.tasks import repeat_every
import os
import uvicorn

# Lecture de la variable d'environnement WAIT_TIME pour déterminer le temps d'attente entre chaque vérification de mail
load_dotenv()
WAIT_TIME = int(os.environ.get('WAIT_TIME'))


# Définition de la structure d'un mail
class DBMail(BaseModel):
    id: int = None
    send_from: str
    send_to: str
    subject: str
    msg: str
    date: str
    state: str = "waiting"
 

# Initialisation de l'application FastAPI
app = FastAPI()

# Route pour envoyer un mail
@app.post("/post_mail/")
async def post_mail(mail: DBMail):

    with DBConnection().connection as conn:
        cursor = conn.cursor()
        cursor.execute('INSERT INTO mail (send_from, send_to, subject, msg, post_date, statut) VALUES (?, ?, ?, ?, ?, ?)', (mail.send_from, mail.send_to, mail.subject, mail.msg, mail.date, mail.state))
        DBConnection().connection.commit()
        mail_id = cursor.lastrowid  # Get the ID of the inserted row
    return {"mail_id": mail_id}  # Return the ID as a response

@app.get('/mail/{id}/')
async def get_mail(id: int):
    with DBConnection().connection as conn:
        cursor = conn.cursor()
        # Insertion du mail dans la base de données
        cursor.execute('SELECT * FROM mail WHERE id = ?', (id,))
        # Validation de l'opération
        mail = cursor.fetchone()
    if not mail:
        return {"detail": "Mail not found."}
    mail = Mail(mail[1], mail[2], mail[3], mail[4], mail[5], mail[6], mail[0])
    return mail

# Route pour récupérer un mail spécifique
@app.get('/mails/')
async def get_all_mail():
    with DBConnection().connection as conn:
        cursor = conn.cursor()
        # Récupération du mail depuis la base de données
        cursor.execute('SELECT * FROM mail')
        # Transformation du mail en objet Mail
        list_mail_db = cursor.fetchall()
    list_mail = [Mail(mail[1], mail[2], mail[3], mail[4], mail[5], mail[6], mail[0]) for mail in list_mail_db]
    return list_mail

# Route pour récupérer tous les mails
@app.on_event("startup")
@repeat_every(seconds=WAIT_TIME)
def check_mails() -> None:
    '''this function checks the mails that have to be sent and sends them'''
    with DBConnection().connection as conn:
        cursor = conn.cursor()
        # Récupération des mails en attente d'envoi
        cursor.execute('SELECT * FROM mail WHERE statut = "waiting"')
        list_mail = cursor.fetchall()
        # Vérification si le mail doit être envoyé
    for mail in list_mail:
        mail = Mail(mail[1], mail[2], mail[3], mail[4], mail[5], mail[6], mail[0])
        mail_time = datetime.strptime(str(mail.date), "%a %b %d %H:%M:%S %Y")
        
        if mail_time < datetime.now():
            mail.send(cursor)
            DBConnection().connection.commit()



uvicorn.run(app)